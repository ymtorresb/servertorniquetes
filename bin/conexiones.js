var mysql = require('mysql');
var MYSQLCONEXION1 = mysql.createConnection({
    host: '10.16.86.66',
    user: 'kbustemporal',
    password: 'kbustemporal',
    database: 'kbusrecaudodb',
    port: 3306
});
MYSQLCONEXION1.connect(function (error) {
    if (error) {
        console.log(error);
        throw error;
    } else {
        console.log('Conexion correcta.');
    }
});

var MYSQLCONEXION2 = mysql.createConnection({
    host: '10.16.86.66',
    user: 'kbustemporal',
    password: 'kbustemporal',
    database: 'kbusrecaudodb',
    port: 3306
});
MYSQLCONEXION2.connect(function (error) {
    if (error) {
        console.log(error);
        throw error;
    } else {
        console.log('Conexion correcta.');
    }
});
var conexion = 0;
exports.insertarRegistro = function (datos, callback) {
    var devolucion = false;
    try {
        if (conexion === 0) {
            MYSQLCONEXION1.query('INSERT INTO registro (hora,fecha,codigo,orden,estado,valor_estado,idtaquillero,idparada,idtorniquete) VALUES(?, ?, ?,?, ?, ?,?, ?, ?)', datos, function (error, result) {
                if (error) {
                    console.log(error);
                    callback(devolucion);
                } else {
                    devolucion = true;
                    callback(devolucion,datos);
                }
            }
            );
            conexion = 1;
        } else {
            MYSQLCONEXION2.query('INSERT INTO registro (hora,fecha,codigo,orden,estado,valor_estado,idtaquillero,idparada,idtorniquete) VALUES(?, ?, ?,?, ?, ?,?, ?, ?)', datos, function (error, result) {
                if (error) {
                    console.log(error);
                    callback(devolucion);
                } else {
                    devolucion = true;
                    callback(devolucion,datos);
                }
            }
            );
            conexion = 0;
        }
    } catch (exception) {
        console.log(exception);
    }
    ;
};
exports.insertarTotal = function (datos, callback) {
    var devolucion = false;
    try {
        if (conexion === 0) {
            MYSQLCONEXION1.query('INSERT INTO totales (hora,fecha,normal,medio,especial,forzado,giros,tarjeta,idparada,idtorniquete) VALUES(?, ?, ?,?, ?, ?,?, ?, ?,?)', datos, function (error, result) {
                if (error) {
                    console.log(error);
                    callback(devolucion);
                } else {
                    devolucion = true;
                    callback(devolucion);
                }
            }
            );
            conexion = 1;
        } else {
            MYSQLCONEXION2.query('INSERT INTO totales (hora,fecha,normal,medio,especial,forzado,giros,tarjeta,idparada,idtorniquete) VALUES(?, ?, ?,?, ?, ?,?, ?, ?,?)', datos, function (error, result) {
                if (error) {
                    console.log(error);
                    callback(devolucion);
                } else {
                    devolucion = true;
                    callback(devolucion);
                }
            }
            );
            conexion = 0;
        }
    } catch (exception) {
        console.log(exception);
    }
    ;
};